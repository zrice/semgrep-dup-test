// react/no-danger incorrect code
var Hello = <div dangerouslySetInnerHTML={{ __html: "Hello World" }}></div>; // no-danger

// react/no-danger correct code
var Hello = <div>Hello World</div>;

// react/no-danger-with-children incorrect code
React.createElement("div", { dangerouslySetInnerHTML: { __html: "HTML" } }, "Children"); // no-danger-with-children

React.createElement("Hello", { dangerouslySetInnerHTML: { __html: "HTML" } }, "Children"); // no-danger-with-children

// react/no-danger-with-children correct code
React.createElement("div", { dangerouslySetInnerHTML: { __html: "HTML" } });

React.createElement("Hello", { dangerouslySetInnerHTML: { __html: "HTML" } });

React.createElement("div", {}, "Children");

React.createElement("Hello", {}, "Children");
