<div>
  // react/no-danger-with-children incorrect code
  <div>
    <div dangerouslySetInnerHTML={{ __html: "HTML" }}> // no-danger & no-danger-with-children
      Children
    </div>

    <Hello dangerouslySetInnerHTML={{ __html: "HTML" }}> // no-danger-with-children
      Children
    </Hello>
  </div>

  // react/no-danger-with-children correct code
  <div>
    <div dangerouslySetInnerHTML={{ __html: "HTML" }} /> // no-danger

    <Hello dangerouslySetInnerHTML={{ __html: "HTML" }} />

    <div>
      Children
    </div>

    <Hello>
      Children
    </Hello>
  </div>
</div>
